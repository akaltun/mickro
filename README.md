# 白金技术论坛

#### 介绍
新疆白金技术论坛

#### 软件架构
在lusses页面 标题中写了后缀和post数据提交方式 
你可以好好看看post数据提交了什么 
其他的我也不多说了


#### 安装教程

1.  先把代码复制好
2.  然后上传到自己想要的目录
3.  改名称 post数据提交
4.  上传相应的txt资源文件夹 文件
5.  访问标题上的链接就可以OK了

#### 使用说明

1.  先把代码复制好
2.  然后上传到自己想要的目录
3.  改名称 post数据提交
4.  上传相应的txt资源文件夹 文件
5.  访问标题上的链接就可以OK了

#### 参与贡献

1.  padishah
2.  losfer
3.  陌路
4.  uyghan
5. 代码来自网络 我只是想做个 新疆低调分享 程序员


#### 特技

1.  使用 Readme\_en.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
     我这边只添加个维语吧 要不。。。
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
 
### 联系方式 
1876988499  mickro773@qq.com
官方网站
http://akaltun.mysxl.cn  http://akaltun.ltd  http://ww.akaltun.ltd
json api 数据
http://yun.akaltun.ltd    http://ww.akaltun.ltd/json
两个在线后台
http://yun.akaltun.ltd/Web  http://yun.akaltun.ltd/tor